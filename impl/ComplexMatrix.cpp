
#include "ComplexMatrix.hpp"
#include "SquareMatrixAlgebra.hpp"
#include <algorithm>
#include <iostream>
#include <numeric>
#include <utility>

using namespace AlgebraMatrix;

struct ComplexMatrix::ComplexMatrixElemsImpl
{
    vector_vector<std::complex<Real>> elems;
};

template<typename T>
bool
isValidMatrixElems(const vector_vector<T>& elems)
{
    if (elems.empty()) {
        return false;
    } else {
        bool rowsNotEmpty =
            std::none_of(elems.begin(),
                         elems.end(),
                         [](const std::vector<T>& row) { return row.empty(); });
        bool consistent =
            std::all_of(elems.begin() + 1,
                        elems.end(),
                        [&elems](const std::vector<T>& row) {
                            return row.size() == elems.front().size();
                        });
        return rowsNotEmpty && consistent;
    }
}

ComplexMatrix::ComplexMatrix(std::size_t rowsCount,
                             std::size_t columnsCount,
                             ComplexMatrixFiller matrixFiller)
    : m_upMatrixElemsImpl(std::make_unique<ComplexMatrixElemsImpl>())
{
    if ((rowsCount < 1) || (columnsCount < 1)) {
        throw std::invalid_argument("Invalid matrix dimension < 1\n");
    }
    vector_vector<std::complex<Real>> elemsData(
        rowsCount, std::vector<std::complex<Real>>(columnsCount));
    for (auto it1 = elemsData.begin(); it1 != elemsData.end(); it1++) {
        for (auto it2 = it1->begin(); it2 != it1->end(); it2++) {
            *it2 = matrixFiller(std::distance(elemsData.begin(), it1),
                                std::distance(it1->begin(), it2));
        }
    }
    m_upMatrixElemsImpl->elems = std::move(elemsData);
}

ComplexMatrix::ComplexMatrix(const std::vector<std::complex<Real>>& raw)
    : m_upMatrixElemsImpl(std::make_unique<ComplexMatrixElemsImpl>())
{
    if (raw.empty()) {
        throw std::invalid_argument("A raw cannot be empty\n");
    }
    m_upMatrixElemsImpl->elems.push_back(raw);
}

ComplexMatrix::ComplexMatrix(std::vector<std::complex<Real>>&& raw)
    : m_upMatrixElemsImpl(std::make_unique<ComplexMatrixElemsImpl>())
{
    if (raw.empty()) {
        throw std::invalid_argument("A raw cannot be empty\n");
    }
    m_upMatrixElemsImpl->elems.push_back(std::move(raw));
}

ComplexMatrix::ComplexMatrix(const vector_vector<std::complex<Real>>& elems)
    : m_upMatrixElemsImpl(std::make_unique<ComplexMatrixElemsImpl>())
{
    if (!isValidMatrixElems(elems)) {
        throw std::invalid_argument(
            "Invalid matrix, check rows/columns size and emptiness\n");
    }
    m_upMatrixElemsImpl->elems = elems;
}

ComplexMatrix::ComplexMatrix(vector_vector<std::complex<Real>>&& elems)
    : m_upMatrixElemsImpl(std::make_unique<ComplexMatrixElemsImpl>())
{
    if (!isValidMatrixElems(elems)) {
        throw std::invalid_argument(
            "Invalid matrix, check rows/columns size and emptiness\n");
    }
    m_upMatrixElemsImpl->elems = std::move(elems);
}

ComplexMatrix::ComplexMatrix(
    const std::initializer_list<std::vector<std::complex<Real>>>& elems)
    : m_upMatrixElemsImpl(std::make_unique<ComplexMatrixElemsImpl>())
{
    vector_vector<std::complex<Real>> temp(elems);
    if (!isValidMatrixElems(temp)) {
        throw std::invalid_argument(
            "Invalid matrix, check rows/columns size and emptiness\n");
    }
    m_upMatrixElemsImpl->elems = std::move(temp);
}

ComplexMatrix::ComplexMatrix(const ComplexMatrix& other)
    : m_upMatrixElemsImpl(
          std::make_unique<ComplexMatrixElemsImpl>(*other.m_upMatrixElemsImpl))
{
}

ComplexMatrix::ComplexMatrix(ComplexMatrix&& other)
    : m_upMatrixElemsImpl(std::move(other.m_upMatrixElemsImpl))
{
}

ComplexMatrix&
ComplexMatrix::operator=(const ComplexMatrix& other)
{
    if (other.size() != size()) {
        throw std::invalid_argument(
            "Matrix sizes don't match, assignment mathematically impossible\n");
    }
    ComplexMatrix temp(other);
    m_upMatrixElemsImpl = std::exchange(temp.m_upMatrixElemsImpl, nullptr);
    return *this;
}

ComplexMatrix&
ComplexMatrix::operator=(ComplexMatrix&& other)
{
    if (other.size() != size()) {
        throw std::invalid_argument(
            "Matrix sizes don't match, assignment mathematically impossible\n");
    }
    m_upMatrixElemsImpl = std::exchange(other.m_upMatrixElemsImpl, nullptr);
    return *this;
}

ComplexMatrix::~ComplexMatrix() = default;

std::pair<ComplexMatrix::rows, ComplexMatrix::columns>
ComplexMatrix::size() const noexcept
{
    return std::make_pair(m_upMatrixElemsImpl->elems.size(),
                          m_upMatrixElemsImpl->elems.empty()
                              ? 0
                              : m_upMatrixElemsImpl->elems.front().size());
}

std::complex<Real>&
ComplexMatrix::at(std::size_t rowIndex, std::size_t columnIndex)
{
    return m_upMatrixElemsImpl->elems.at(rowIndex).at(columnIndex);
}

const std::complex<Real>&
ComplexMatrix::at(std::size_t rowIndex, std::size_t columnIndex) const
{
    return m_upMatrixElemsImpl->elems.at(rowIndex).at(columnIndex);
}

std::vector<std::complex<Real>>
ComplexMatrix::row(std::size_t rowIndex) const
{
    return m_upMatrixElemsImpl->elems.at(rowIndex);
}

std::vector<std::complex<Real>>
ComplexMatrix::column(std::size_t columnIndex) const
{
    std::vector<std::complex<Real>> columnVect;
    for (const auto& it : m_upMatrixElemsImpl->elems) {
        columnVect.insert(columnVect.end(), it.at(columnIndex));
    }
    return columnVect;
}

bool
ComplexMatrix::invertible() const
{
    return determinant(*this) != static_cast<Real>(0);
}

ComplexMatrix
ComplexMatrix::operator-() const
{
    ComplexMatrix result(*this);
    for (auto& it1 : result.m_upMatrixElemsImpl->elems) {
        for (auto& it2 : it1) {
            it2 = -it2;
        }
    }
    return result;
}

namespace AlgebraMatrix {
bool
operator==(const ComplexMatrix& lhs, const ComplexMatrix& rhs)
{
    return lhs.m_upMatrixElemsImpl->elems == rhs.m_upMatrixElemsImpl->elems;
}

ComplexMatrix
operator+(const ComplexMatrix& lhs, const ComplexMatrix& rhs)
{
    if (lhs.size() != rhs.size()) {
        throw std::invalid_argument(
            "Matrix sizes don't match, addition mathematically impossible\n");
    }
    auto resultElems(lhs.m_upMatrixElemsImpl->elems);
    const auto& rhsElems = rhs.m_upMatrixElemsImpl->elems;
    for (auto it1 = resultElems.begin(); it1 != resultElems.end(); it1++) {
        std::transform(
            it1->cbegin(),
            it1->cend(),
            rhsElems.at(std::distance(resultElems.begin(), it1)).cbegin(),
            it1->begin(),
            std::plus<>{});
    }
    return ComplexMatrix(std::move(resultElems));
}

ComplexMatrix
operator*(const ComplexMatrix& lhs, const ComplexMatrix& rhs)
{
    if (lhs.size().second != rhs.size().first) {
        throw std::invalid_argument(
            "In A * B: A must have as many columns as B has rows. Otherwise, "
            "the multiplication is mathematically impossible\n");
    }
    vector_vector<std::complex<Real>> resultElems(
        lhs.size().first, std::vector<std::complex<Real>>(rhs.size().second));
    const auto& lhsResult = lhs.m_upMatrixElemsImpl->elems;
    for (auto it1 = resultElems.begin(); it1 != resultElems.cend(); it1++) {
        for (auto it2 = it1->begin(); it2 != it1->cend(); it2++) {
            auto&& columnMatrixRhs =
                rhs.column(std::distance(it1->begin(), it2));
            *it2 = std::inner_product(
                lhsResult.at(std::distance(resultElems.begin(), it1)).cbegin(),
                lhsResult.at(std::distance(resultElems.begin(), it1)).cend(),
                columnMatrixRhs.cbegin(),
                std::complex<Real>());
        }
    }
    return ComplexMatrix(std::move(resultElems));
}

ComplexMatrix
operator*(const ComplexMatrix& mat, const std::complex<Real>& scalar)
{
    auto resultElems(mat.m_upMatrixElemsImpl->elems);
    auto doScalarTimesRow = [&scalar](std::vector<std::complex<Real>>& row) {
        std::for_each(row.begin(),
                      row.end(),
                      [&scalar](std::complex<Real>& elem) { elem *= scalar; });
    };
    std::for_each(resultElems.begin(), resultElems.end(), doScalarTimesRow);
    return ComplexMatrix(std::move(resultElems));
}

ComplexMatrix
operator/(const ComplexMatrix& lhs, const ComplexMatrix& rhs)
{
    auto&& rhsInv = inverse(rhs);
    return lhs * rhsInv;
}
} // namespace AlgebraMatrix
