#include "MatrixGeneralAlgebra.hpp"
#include "ComplementaryRangeFrom.hpp"

using namespace AlgebraMatrix;

ComplexMatrix
AlgebraMatrix::transpose(const ComplexMatrix& mat)
{
    std::vector<std::vector<std::complex<Real>>> transposeElems(
        mat.size().second);
    for (auto it = transposeElems.begin(); it != transposeElems.end(); it++) {
        auto&& transposeRow =
            mat.column(std::distance(transposeElems.begin(), it));
        *it = std::move(transposeRow);
    }
    return ComplexMatrix(std::move(transposeElems));
}

std::vector<std::complex<Real>>::iterator
eraseColumnsElemsInRow(std::vector<std::complex<Real>>& row,
                       std::vector<std::size_t>& columnsToDeleteIndexes);

std::vector<std::vector<std::complex<Real>>>
createValidSubmatrixElems(const ComplexMatrix& mat,
                          std::vector<std::size_t>& rowsToDeleteIndexes);

ComplexMatrix
AlgebraMatrix::extractSubmatrix(
    const ComplexMatrix& mat,
    std::vector<std::size_t>&& rowsToDeleteIndexes,
    std::vector<std::size_t>&& columnsToDeleteIndexes)
{
    auto&& subMatrixElems = createValidSubmatrixElems(mat, rowsToDeleteIndexes);
    auto&& rowsToKeepIndexes =
        complementaryRangeFrom(rowsToDeleteIndexes, mat.size().first);
    for (auto it = subMatrixElems.begin(); it != subMatrixElems.end(); it++) {
        auto&& rowToAdd = mat.row(
            rowsToKeepIndexes.at(std::distance(subMatrixElems.begin(), it)));
        eraseColumnsElemsInRow(rowToAdd, columnsToDeleteIndexes);
        *it = std::move(rowToAdd);
    }
    return ComplexMatrix(std::move(subMatrixElems));
}

std::vector<std::complex<Real>>::iterator
eraseColumnsElemsInRow(std::vector<std::complex<Real>>& row,
                       std::vector<std::size_t>& columnsToDeleteIndexes)
{
    // To avoid memory error when erasing element in row to add, the indexes
    // are sorted in reverse
    std::sort(columnsToDeleteIndexes.begin(),
              columnsToDeleteIndexes.end(),
              std::greater<std::size_t>());
    if (columnsToDeleteIndexes.front() >= row.size()) {
        throw std::out_of_range("Column index out of range\n");
    }
    // Then removing the duplicates is also extremly important
    std::unique(columnsToDeleteIndexes.begin(), columnsToDeleteIndexes.end());
    decltype(row.begin()) it;
    for (auto& index : columnsToDeleteIndexes) {
        it = row.erase(row.begin() + index);
    }
    return it;
}

std::vector<std::vector<std::complex<Real>>>
createValidSubmatrixElems(const ComplexMatrix& mat,
                          std::vector<std::size_t>& rowsToDeleteIndexes)
{
    std::sort(rowsToDeleteIndexes.begin(), rowsToDeleteIndexes.end());
    std::unique(rowsToDeleteIndexes.begin(), rowsToDeleteIndexes.end());
    if (rowsToDeleteIndexes.front() >= mat.size().first) {
        throw std::out_of_range("Row index out of range\n");
    }
    return std::vector<std::vector<std::complex<Real>>>(
        mat.size().first - rowsToDeleteIndexes.size());
}
