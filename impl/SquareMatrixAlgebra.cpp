#include "SquareMatrixAlgebra.hpp"
#include "MatrixGeneralAlgebra.hpp"

using namespace AlgebraMatrix;

std::complex<Real>
AlgebraMatrix::determinant(const ComplexMatrix& mat)
{
    if (!mat.square()) {
        throw std::invalid_argument(
            "Cannot calculate the determinant of a non square matrix\n");
    }
    if (mat.size() == decltype(mat.size()){ 1, 1 }) {
        return std::complex<Real>(mat.at(0, 0));
    }
    std::complex<Real> total = 0;
    for (std::size_t columnIndex = 0; columnIndex < mat.size().second;
         columnIndex++) {
        auto&& K = extractSubmatrix(mat, { 0 }, { columnIndex });
        int sign = columnIndex % 2 ? -1 : 1;
        total +=
            static_cast<Real>(sign) * mat.at(0, columnIndex) * determinant(K);
    }
    return total;
}

ComplexMatrix
AlgebraMatrix::adjugate(const ComplexMatrix& mat)
{
    auto minor = [&mat](std::size_t i, std::size_t j) {
        int sign = (i + j) % 2 ? -1 : 1;
        auto&& minorSubmatrix = extractSubmatrix(mat, { i }, { j });
        return static_cast<Real>(sign) * determinant(minorSubmatrix);
    };
    auto&& comatrix = ComplexMatrix(mat.size().first, mat.size().second, minor);
    return transpose(comatrix);
}

ComplexMatrix
AlgebraMatrix::inverse(const ComplexMatrix& mat)
{
    if (!mat.invertible()) {
        throw std::invalid_argument("Cannot invert an non invertible matrix\n");
    }
    auto&& adj = adjugate(mat);
    return static_cast<Real>(1) / determinant(mat) * adj;
}
