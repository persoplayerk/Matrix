#pragma once
#include <numeric>
#include <vector>

namespace AlgebraMatrix {
template<typename T, std::enable_if_t<std::is_integral_v<T>>* = nullptr>
std::vector<T>
complementaryRangeFrom(const std::vector<T>& range, std::size_t rangeSize)
{
    std::vector<T> complementaryRange(rangeSize);
    std::iota(complementaryRange.begin(), complementaryRange.end(), 0);
    auto newEnd = std::remove_if(
        complementaryRange.begin(),
        complementaryRange.end(),
        [&range](T index) {
            return std::find(range.begin(), range.end(), index) != range.end();
        });
    complementaryRange.erase(newEnd, complementaryRange.end());
    return complementaryRange;
}
} // namespace AlgebraMatrix
