#pragma once
#include "ComplexMatrix.hpp"

namespace AlgebraMatrix {

ComplexMatrix
transpose(const ComplexMatrix& mat);

ComplexMatrix
extractSubmatrix(const ComplexMatrix& mat,
                 std::vector<std::size_t>&& rowsToDeleteIndexes,
                 std::vector<std::size_t>&& columnsToDeleteIndexes);
} // namespace AlgebraMatrix
