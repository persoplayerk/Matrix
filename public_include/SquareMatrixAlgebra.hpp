#pragma once
#include "ComplexMatrix.hpp"

namespace AlgebraMatrix {

std::complex<Real>
determinant(const ComplexMatrix& mat);

ComplexMatrix
adjugate(const ComplexMatrix& mat);

ComplexMatrix
inverse(const ComplexMatrix& mat);
} // namespace AlgebraMatrix
