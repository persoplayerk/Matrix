#pragma once
#include <complex>
#include <functional>
#include <memory>
#include <vector>

namespace AlgebraMatrix {

using Real = long double;
template<typename T>
using vector_vector = std::vector<std::vector<T>>;
using ComplexMatrixFiller =
    std::function<std::complex<Real>(std::size_t, std::size_t)>;
static auto defaultFiller = [](std::size_t, std::size_t) { return 0; };

class ComplexMatrix
{
public:
    ComplexMatrix(std::size_t rowsCount,
                  std::size_t columnsCount,
                  ComplexMatrixFiller matrixFiller = defaultFiller);
    explicit inline ComplexMatrix(
        std::size_t dimension = 1,
        ComplexMatrixFiller matrixFiller = defaultFiller)
        : ComplexMatrix(dimension, dimension, matrixFiller)
    {
    }
    ComplexMatrix(const std::vector<std::complex<Real>>& raw);
    ComplexMatrix(std::vector<std::complex<Real>>&& raw);
    ComplexMatrix(const std::vector<std::vector<std::complex<Real>>>& elems);
    ComplexMatrix(std::vector<std::vector<std::complex<Real>>>&& elems);
    ComplexMatrix(
        const std::initializer_list<std::vector<std::complex<Real>>>& elems);
    ComplexMatrix(const ComplexMatrix& other);
    ComplexMatrix(ComplexMatrix&& other);
    ComplexMatrix& operator=(const ComplexMatrix& other);
    ComplexMatrix& operator=(ComplexMatrix&& other);
    virtual ~ComplexMatrix();

    using rows = std::size_t;
    using columns = std::size_t;
    std::pair<rows, columns> size() const noexcept;
    std::complex<Real>& at(std::size_t rowIndex, std::size_t columnIndex);
    const std::complex<Real>& at(std::size_t rowIndex,
                                 std::size_t columnIndex) const;
    std::vector<std::complex<Real>> row(std::size_t rowIndex) const;
    std::vector<std::complex<Real>> column(std::size_t columnIndex) const;
    inline bool square() const noexcept
    {
        return size().first == size().second;
    }
    bool invertible() const;

    friend bool operator==(const ComplexMatrix& lhs, const ComplexMatrix& rhs);
    friend inline bool operator!=(const ComplexMatrix& lhs,
                                  const ComplexMatrix& rhs)
    {
        return !(lhs == rhs);
    }
    ComplexMatrix inline operator+() const { return *this; }
    ComplexMatrix operator-() const;
    friend ComplexMatrix operator+(const ComplexMatrix& lhs,
                                   const ComplexMatrix& rhs);
    friend inline ComplexMatrix operator-(const ComplexMatrix& lhs,
                                          const ComplexMatrix& rhs)
    {
        return lhs + (-rhs);
    }
    friend ComplexMatrix operator*(const ComplexMatrix& lhs,
                                   const ComplexMatrix& rhs);
    friend ComplexMatrix operator*(const ComplexMatrix& mat,
                                   const std::complex<Real>& scalar);
    friend inline ComplexMatrix operator*(const std::complex<Real>& scalar,
                                          const ComplexMatrix& mat)
    {
        return mat * scalar;
    }
    friend ComplexMatrix operator/(const ComplexMatrix& lhs,
                                   const ComplexMatrix& rhs);
    friend inline ComplexMatrix operator/(const ComplexMatrix& mat,
                                          const std::complex<Real>& scalar)
    {
        return mat * (static_cast<Real>(1) / scalar);
    }
    friend inline ComplexMatrix operator/(const std::complex<Real>& scalar,
                                          const ComplexMatrix& mat)
    {
        return mat * (static_cast<Real>(1) / scalar);
    }
    inline ComplexMatrix& operator+=(const ComplexMatrix& other)
    {
        return *this = *this + other;
    }
    inline ComplexMatrix& operator-=(const ComplexMatrix& other)
    {
        return *this = *this - other;
    }
    inline ComplexMatrix& operator*=(const ComplexMatrix& other)
    {
        return *this = *this * other;
    }
    inline ComplexMatrix& operator*=(const std::complex<Real>& scalar)
    {
        return *this = *this * scalar;
    }
    inline ComplexMatrix& operator/=(const ComplexMatrix& other)
    {
        return *this = *this / other;
    }
    inline ComplexMatrix& operator/=(const std::complex<Real>& scalar)
    {
        return *this = *this / scalar;
    }

private:
    struct ComplexMatrixElemsImpl;
    std::unique_ptr<ComplexMatrixElemsImpl> m_upMatrixElemsImpl;
};

inline ComplexMatrix
cIdentity(std::size_t dim)
{
    auto kronecker = [](std::size_t i, std::size_t j) {
        return std::complex<Real>(i == j);
    };
    return ComplexMatrix(dim, kronecker);
}

} // namespace AlgebraMatrix
