FROM alpine/git:latest
WORKDIR /test
RUN git clone https://github.com/google/googletest.git

FROM ubuntu:focal
WORKDIR /test
COPY --from=0 /test/ .
ARG DEBIAN_FRONTEND=noninteractive

ENV TZ=Etc/UTC
RUN apt update && \
    apt install -y build-essential wget gcovr
RUN wget -c https://github.com/Kitware/CMake/releases/download/v3.23.1/cmake-3.23.1-linux-x86_64.tar.gz
RUN tar xzf cmake-3.23.1-linux-x86_64.tar.gz && \
    cd cmake-3.23.1-linux-x86_64 && \
    cp -r bin/ share/ /usr/local/ && \
    rm -rf cmake-*
RUN cd googletest && \
    mkdir build && cd build && cmake .. && cmake --build . && cmake --install .
ENTRYPOINT /bin/bash
