#pragma once
#include <gtest/gtest.h>

namespace AlgebraMatrix {
class GeneralAlgebraUnitTests : public ::testing::Test
{
protected:
    GeneralAlgebraUnitTests() = default;
    virtual ~GeneralAlgebraUnitTests() = default;
};
} // namespace AlgebraMatrix
