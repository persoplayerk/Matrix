#pragma once
#include <gtest/gtest.h>

namespace AlgebraMatrix {
class ComplexMatrixUnitTests : public ::testing::Test
{
protected:
    ComplexMatrixUnitTests() = default;
    virtual ~ComplexMatrixUnitTests() = default;
};
} // namespace AlgebraMatrix
