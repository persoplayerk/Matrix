#include "ComplexMatrixUnitTests.hpp"
#include "ComplexMatrix.hpp"

constexpr std::size_t DIM2 = 2;
constexpr std::size_t DIM3 = 3;

using namespace AlgebraMatrix;
using ComplexMatrixUPtr = std::unique_ptr<ComplexMatrix>;

TEST_F(ComplexMatrixUnitTests, createDefaultMatrix)
{
    ComplexMatrixUPtr defaultMatrix = nullptr;
    defaultMatrix = std::make_unique<ComplexMatrix>();
    EXPECT_TRUE(defaultMatrix);
    defaultMatrix.reset(nullptr);
    EXPECT_FALSE(defaultMatrix);
}

TEST_F(ComplexMatrixUnitTests, checkDefaultSize)
{
    ComplexMatrixUPtr defaultMatrix = nullptr;
    defaultMatrix = std::make_unique<ComplexMatrix>();
    decltype(defaultMatrix->size()) expected{ 1, 1 };
    EXPECT_EQ(defaultMatrix->size(), expected);
}

TEST_F(ComplexMatrixUnitTests, createRectangleMatrixFromSize)
{
    ComplexMatrixUPtr M = nullptr;
    M = std::make_unique<ComplexMatrix>(DIM3, DIM2);
    decltype(M->size()) expected{ 3, 2 };
    EXPECT_EQ(M->size(), expected);
}

TEST_F(ComplexMatrixUnitTests, createSquareMatrixFromSize)
{
    ComplexMatrixUPtr M = nullptr;
    M = std::make_unique<ComplexMatrix>(DIM3);
    decltype(M->size()) expected{ 3, 3 };
    EXPECT_EQ(M->size(), expected);
}

TEST_F(ComplexMatrixUnitTests, throwOnInvalidSize)
{
    ComplexMatrixUPtr M = nullptr;
    EXPECT_THROW(M = std::make_unique<ComplexMatrix>(0), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, lookUpAnElement)
{
    ComplexMatrixUPtr M = nullptr;
    M = std::make_unique<ComplexMatrix>(DIM3);
    EXPECT_EQ(M->at(1, 1), std::complex<Real>(0));
}

TEST_F(ComplexMatrixUnitTests, throwOnlookUpElementOutOfRange)
{
    ComplexMatrixUPtr M = nullptr;
    M = std::make_unique<ComplexMatrix>(DIM3);
    EXPECT_THROW(M->at(DIM3, DIM3), std::out_of_range);
}

TEST_F(ComplexMatrixUnitTests, lookUpARow)
{
    std::vector<std::vector<std::complex<Real>>> elems = { { 1, 2, 3 },
                                                           { 4, 5, 6 } };
    ComplexMatrix M(elems);
    for (int i = 0; i < M.size().first; i++) {
        EXPECT_EQ(std::move(M.row(i)), elems.at(i));
    }
}

TEST_F(ComplexMatrixUnitTests, throwOnLookUpRowOutOfRange)
{
    std::vector<std::vector<std::complex<Real>>> elems = { { 1, 2, 3 },
                                                           { 4, 5, 6 } };
    ComplexMatrix M(elems);
    EXPECT_THROW(M.row(M.size().first), std::out_of_range);
}

TEST_F(ComplexMatrixUnitTests, lookUpAColumn)
{
    std::vector<std::vector<std::complex<Real>>> elems = { { 1, 2, 3 },
                                                           { 4, 5, 6 } };
    ComplexMatrix M(elems);
    std::vector<std::vector<std::complex<Real>>> elemsInColumn = { { 1, 4 },
                                                                   { 2, 5 },
                                                                   { 3, 6 } };
    for (int i = 0; i < M.size().second; i++) {
        EXPECT_EQ(std::move(M.column(i)), elemsInColumn.at(i));
    }
}

TEST_F(ComplexMatrixUnitTests, throwOnLookUpAColumnOutOfRange)
{
    std::vector<std::vector<std::complex<Real>>> elems = { { 1, 2, 3 },
                                                           { 4, 5, 6 } };
    ComplexMatrix M(elems);
    EXPECT_THROW(M.column(M.size().second), std::out_of_range);
}

TEST_F(ComplexMatrixUnitTests, createSpecficallyFilledMatrix)
{
    auto kronecker = [](std::size_t i, std::size_t j) {
        return std::complex<Real>(i == j);
    };
    ComplexMatrixUPtr identityMatrix = nullptr;
    identityMatrix = std::make_unique<ComplexMatrix>(DIM3, kronecker);
    for (int i = 0; i < identityMatrix->size().first; i++) {
        for (int j = 0; j < identityMatrix->size().second; j++) {
            EXPECT_EQ(identityMatrix->at(i, j), std::complex<Real>(i == j));
        }
    }
}

TEST_F(ComplexMatrixUnitTests, createRowMatrixFromVector)
{
    ComplexMatrixUPtr rawMatrix = nullptr;
    // Create a raw whose the dimension is (1, 3) and filled with 2
    std::vector<std::complex<Real>> raw(DIM3, 2);
    rawMatrix = std::make_unique<ComplexMatrix>(raw);
    EXPECT_TRUE(rawMatrix);
    EXPECT_EQ(rawMatrix->size(),
              std::make_pair(static_cast<std::size_t>(1), DIM3));
}

TEST_F(ComplexMatrixUnitTests, throwOnEmptyRawVector)
{
    std::vector<std::complex<Real>> raw;
    EXPECT_THROW(ComplexMatrix rawMatrix(raw), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, createRowMatrixFromVectorMove)
{
    ComplexMatrixUPtr rawMatrix = nullptr;
    // Create a raw whose the dimension is (1, 3) and filled with 2
    std::vector<std::complex<Real>> raw(DIM3, 2);
    rawMatrix = std::make_unique<ComplexMatrix>(std::move(raw));
    EXPECT_TRUE(rawMatrix);
    EXPECT_EQ(rawMatrix->size(),
              std::make_pair(static_cast<std::size_t>(1), DIM3));
    EXPECT_FALSE(raw.size());
}

TEST_F(ComplexMatrixUnitTests, throwOnEmptyRawVectorMove)
{
    std::vector<std::complex<Real>> raw;
    EXPECT_THROW(ComplexMatrix rawMatrix(std::move(raw)),
                 std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, createMatrixFromVectorVector)
{
    ComplexMatrixUPtr identityMatrix = nullptr;
    identityMatrix = std::make_unique<ComplexMatrix>(
        std::vector<std::vector<std::complex<Real>>>(
            { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } }));
    for (int i = 0; i < identityMatrix->size().first; i++) {
        for (int j = 0; j < identityMatrix->size().second; j++) {
            EXPECT_EQ(identityMatrix->at(i, j), std::complex<Real>(i == j));
        }
    }
}

TEST_F(ComplexMatrixUnitTests, throwOnInvalidVectorVector)
{
    vector_vector<std::complex<Real>> invalid;
    EXPECT_THROW(ComplexMatrix mat(invalid), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, createMatrixFromVectorVectorMove)
{
    ComplexMatrixUPtr identityMatrix = nullptr;
    std::vector<std::vector<std::complex<Real>>> elems(
        { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } });
    identityMatrix = std::make_unique<ComplexMatrix>(std::move(elems));
    for (int i = 0; i < identityMatrix->size().first; i++) {
        for (int j = 0; j < identityMatrix->size().second; j++) {
            EXPECT_EQ(identityMatrix->at(i, j), std::complex<Real>(i == j));
        }
    }
    // Expect elems is valid but empty
    EXPECT_FALSE(elems.size());
}

TEST_F(ComplexMatrixUnitTests, throwOnInvalidVectorVectorMove)
{
    vector_vector<std::complex<Real>> invalid;
    EXPECT_THROW(ComplexMatrix mat(std::move(invalid)), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, createMatrixFromInitializerList)
{
    ComplexMatrix identityMatrix = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
    for (int i = 0; i < identityMatrix.size().first; i++) {
        for (int j = 0; j < identityMatrix.size().second; j++) {
            EXPECT_EQ(identityMatrix.at(i, j), std::complex<Real>(i == j));
        }
    }
}

TEST_F(ComplexMatrixUnitTests, throwOnInconsistentRowsSize)
{
    auto instantiate = []() {
        ComplexMatrix M = { { 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
    };
    EXPECT_THROW(instantiate(), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, compareEqualMatrixes)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix M2 = { { 1, 1 }, { 1, 1 } };
    EXPECT_EQ(M1, M2);
}

TEST_F(ComplexMatrixUnitTests, compareNonEqualMatrixes)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    EXPECT_NE(M1, I2);
}

TEST_F(ComplexMatrixUnitTests, copyConstructor)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix M2(M1);
    EXPECT_EQ(M1, M2);
}

TEST_F(ComplexMatrixUnitTests, moveConstructor)
{
    auto instantiate = []() {
        ComplexMatrixFiller lineNumber = [](std::size_t line, std::size_t) {
            return line;
        };
        ComplexMatrixUPtr M = std::make_unique<ComplexMatrix>(2, lineNumber);
        return *M;
    };
    ComplexMatrix M1(std::move(instantiate()));
    EXPECT_EQ(M1, instantiate());
}

TEST_F(ComplexMatrixUnitTests, copyAssignment)
{
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    ComplexMatrix I2P(2);
    EXPECT_NE(I2P, I2);
    I2P = I2;
    EXPECT_EQ(I2P, I2);
}

TEST_F(ComplexMatrixUnitTests, copySelfAssignment)
{
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    ComplexMatrix save(I2);
    I2 = I2;
    EXPECT_TRUE(&I2);
    EXPECT_EQ(I2, save);
}

TEST_F(ComplexMatrixUnitTests, throwOnSizeDiffAssignment)
{
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    ComplexMatrix I3 = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
    EXPECT_THROW(I2 = I3, std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, moveAssignment)
{
    auto instantiate = []() {
        ComplexMatrixFiller lineNumber = [](std::size_t line, std::size_t) {
            return line;
        };
        ComplexMatrixUPtr M = std::make_unique<ComplexMatrix>(2, lineNumber);
        return *M;
    };
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    EXPECT_NE(M1, instantiate());
    M1 = std::move(instantiate());
    EXPECT_EQ(M1, instantiate());
}

TEST_F(ComplexMatrixUnitTests, moveSelfAssignment)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix save(M1);
    M1 = std::move(M1);
    EXPECT_EQ(M1, save);
}

TEST_F(ComplexMatrixUnitTests, throwOnSizeDiffMoveAssignment)
{
    ComplexMatrix M1(2);
    EXPECT_THROW(M1 = std::move(ComplexMatrix(3)), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, unaryPlus)
{
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    EXPECT_EQ(+I2, I2);
}

TEST_F(ComplexMatrixUnitTests, unaryMinus)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix expected = { { -1, -1 }, { -1, -1 } };
    EXPECT_EQ(-M1, expected);
}

TEST_F(ComplexMatrixUnitTests, addTwoMatrixes)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix I2(std::move(cIdentity(DIM2)));
    ComplexMatrix expectedAddingResult = { { 2, 1 }, { 1, 2 } };
    ComplexMatrix actualAdditionResult(2);
    actualAdditionResult = M1 + I2;
    EXPECT_EQ(actualAdditionResult, expectedAddingResult);
    EXPECT_THROW(M1 + ComplexMatrix(3), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, substractTwoMatrixes)
{
    ComplexMatrix M1 = { { 2, 2 }, { 2, 2 } };
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    ComplexMatrix expectedSubstractionResult = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix actualSubstractionResult(2);
    actualSubstractionResult = M1 - I2;
    EXPECT_EQ(actualSubstractionResult, expectedSubstractionResult);
}

TEST_F(ComplexMatrixUnitTests, multiplyTwoSquareMatrixes)
{
    ComplexMatrix M1 = { { 1, 2 }, { 3, 4 } };
    ComplexMatrix M2 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix expectedM1TimesM2 = { { 5, 4 }, { 11, 10 } };
    ComplexMatrix expectedM2TimesM1 = { { 7, 10 }, { 5, 8 } };
    ComplexMatrix actualM1TimesM2(2), actualM2TimesM1(2);
    actualM1TimesM2 = M1 * M2;
    actualM2TimesM1 = M2 * M1;
    EXPECT_EQ(actualM1TimesM2, expectedM1TimesM2);
    EXPECT_EQ(actualM2TimesM1, expectedM2TimesM1);
    EXPECT_NE(actualM1TimesM2, actualM2TimesM1);
}

TEST_F(ComplexMatrixUnitTests, multiplyTwoNonSquareMatrixes)
{
    ComplexMatrix M1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
    ComplexMatrix M2 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix expectedM1TimesM2 = { { 5, 4 }, { 11, 10 }, { 17, 16 } };
    ComplexMatrix actualM1TimesM2(3, 2);
    EXPECT_NO_THROW({ actualM1TimesM2 = M1 * M2; });
    EXPECT_EQ(actualM1TimesM2, expectedM1TimesM2);
    EXPECT_THROW(M2 * M1, std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, multiplyMatrixPerScalar)
{
    float scalar = 2.5;
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix result = { { 2.5, 2.5 }, { 2.5, 2.5 } };
    EXPECT_EQ(2.5 * M1, result);
    EXPECT_EQ(M1 * 2.5, result);
}

TEST_F(ComplexMatrixUnitTests, matrixAdditionAssignement)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix M2 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix expectedResult = { { 2, 3 }, { 3, 2 } };
    EXPECT_EQ(M1 += M2, expectedResult);
}

TEST_F(ComplexMatrixUnitTests, matrixSubstractionAssignement)
{
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix M2 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix expectedResult = { { 0, -1 }, { -1, 0 } };
    EXPECT_EQ(M1 -= M2, expectedResult);
}

TEST_F(ComplexMatrixUnitTests, matrixMultiplicationAssignment)
{
    ComplexMatrix M1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
    ComplexMatrix M2 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix expectedM1TimesM2 = { { 5, 4 }, { 11, 10 }, { 17, 16 } };
    EXPECT_EQ(M1 *= M2, expectedM1TimesM2);
    EXPECT_THROW(M2 *= M1, std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, matrixScalarMultiplicationAssignement)
{
    float scalar = 2.5;
    ComplexMatrix M1 = { { 1, 1 }, { 1, 1 } };
    ComplexMatrix result = { { 2.5, 2.5 }, { 2.5, 2.5 } };
    EXPECT_EQ(M1 *= scalar, result);
}

TEST_F(ComplexMatrixUnitTests, squareMatrixTest)
{
    ComplexMatrix M1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
    ComplexMatrix M2 = { { 1, 2 }, { 2, 1 } };
    EXPECT_FALSE(M1.square());
    EXPECT_TRUE(M2.square());
}

TEST_F(ComplexMatrixUnitTests, invertibleTest)
{
    ComplexMatrix M1 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix M2 = { { 2, -1, -2 }, { 6, -1, 1 }, { 4, 5, 3 } };
    EXPECT_TRUE(M1.invertible() && M2.invertible());
    ComplexMatrix M3 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
    EXPECT_FALSE(M3.invertible());
}

TEST_F(ComplexMatrixUnitTests, throwOnInvertibleTestToNonSquareMatrix)
{
    ComplexMatrix nsqMat = { { 2, -1, -2 }, { 6, -1, 1 } };
    EXPECT_THROW(nsqMat.invertible(), std::invalid_argument);
}

TEST_F(ComplexMatrixUnitTests, matrixDivision)
{
    ComplexMatrix M1 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    EXPECT_EQ(M1 / M1, I2);
}

TEST_F(ComplexMatrixUnitTests, matrixScalarDivision)
{
    ComplexMatrix M1 = { { 2.5, 2.5 }, { 2.5, 2.5 } };
    std::complex<Real> scalar = 2.5;
    ComplexMatrix result = { { 1, 1 }, { 1, 1 } };
    EXPECT_EQ(M1 / scalar, result);
}

TEST_F(ComplexMatrixUnitTests, matrixDivisionAssignment)
{
    ComplexMatrix M1 = { { 1, 2 }, { 2, 1 } };
    ComplexMatrix I2 = { { 1, 0 }, { 0, 1 } };
    EXPECT_EQ(M1 /= M1, I2);
}

TEST_F(ComplexMatrixUnitTests, matrixScalarDivisionAssignement)
{
    ComplexMatrix M1 = { { 2.5, 2.5 }, { 2.5, 2.5 } };
    std::complex<Real> scalar = 2.5;
    ComplexMatrix result = { { 1, 1 }, { 1, 1 } };
    EXPECT_EQ(M1 /= scalar, result);
}

TEST_F(ComplexMatrixUnitTests, throwOnNonSquarematrixDivision)
{
    ComplexMatrix M1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
    ComplexMatrix M1Save = M1;
    EXPECT_THROW(M1 / M1, std::invalid_argument);
    EXPECT_THROW(M1 /= M1, std::invalid_argument);
    // M1 is must be still in a valid state
    EXPECT_EQ(M1, M1Save);
}
