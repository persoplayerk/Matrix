#pragma once
#include <gtest/gtest.h>

namespace AlgebraMatrix {
class SquareMatrixAlgebraUnitTests : public ::testing::Test
{
protected:
    SquareMatrixAlgebraUnitTests() = default;
    virtual ~SquareMatrixAlgebraUnitTests() = default;
};
} // namespace AlgebraMatrix
