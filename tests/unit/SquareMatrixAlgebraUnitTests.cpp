#include "SquareMatrixAlgebraUnitTests.hpp"
#include "SquareMatrixAlgebra.hpp"

using namespace AlgebraMatrix;

TEST_F(SquareMatrixAlgebraUnitTests, squareMatrixDeterminant)
{
    ComplexMatrix sqMat = { { 2, -1, -2 }, { 6, -1, 1 }, { 4, 5, 3 } };
    EXPECT_EQ(determinant(sqMat), static_cast<Real>(-70));
}

TEST_F(SquareMatrixAlgebraUnitTests, throwOnNonSquareMatrixDeterminant)
{
    ComplexMatrix nsqMat = { { 2, -1, -2 }, { 6, -1, 1 } };
    EXPECT_THROW(determinant(nsqMat), std::invalid_argument);
}

TEST_F(SquareMatrixAlgebraUnitTests, squareMatrixAdjugate)
{
    ComplexMatrix sqMat = { { -3, 2, -5 }, { -1, 0, -2 }, { 3, -4, 1 } };
    ComplexMatrix expectedResult = { { -8, 18, -4 },
                                     { -5, 12, -1 },
                                     { 4, -6, 2 } };
    auto adj = adjugate(sqMat);
    EXPECT_EQ(adjugate(sqMat), expectedResult);
}

TEST_F(SquareMatrixAlgebraUnitTests, throwOnNonSquareMatrixAdjugate)
{
    ComplexMatrix nsqMat = { { 2, -1, -2 }, { 6, -1, 1 } };
    EXPECT_THROW(adjugate(nsqMat), std::invalid_argument);
}

TEST_F(SquareMatrixAlgebraUnitTests, squareMatrixInverse)
{
    ComplexMatrix sqMat = { { -3, 2, -5 }, { -1, 0, -2 }, { 3, -4, 1 } };
    ComplexMatrix&& sqMatInv = inverse(sqMat);
    ComplexMatrix identityMatrix = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
    auto&& actual = sqMat * sqMatInv;
    for (unsigned int i = 0; i < actual.size().first; i++) {
        for (unsigned int j = 0; j < actual.size().second; j++) {
            EXPECT_NEAR(
                actual.at(i, j).real(), identityMatrix.at(i, j).real(), 1e-18);
            EXPECT_NEAR(
                actual.at(i, j).imag(), identityMatrix.at(i, j).imag(), 1e-18);
        }
    }
}

TEST_F(SquareMatrixAlgebraUnitTests, throwOnNonInvertibleSquarteMatrixInverse)
{
    ComplexMatrix nonInvSqMat = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
    EXPECT_THROW(inverse(nonInvSqMat), std::invalid_argument);
}

TEST_F(SquareMatrixAlgebraUnitTests, throwOnNonSquareMatrixInverse)
{
    ComplexMatrix nonSqMat = { { 1, 2, 3 }, { 4, 5, 6 } };
    EXPECT_THROW(inverse(nonSqMat), std::invalid_argument);
}
