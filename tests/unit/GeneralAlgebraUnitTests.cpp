#include "GeneralAlgebraUnitTests.hpp"
#include "MatrixGeneralAlgebra.hpp"

using namespace AlgebraMatrix;

TEST_F(GeneralAlgebraUnitTests, transposeMatrix)
{
    ComplexMatrix square = { { 1, 2 }, { 3, 4 } };
    ComplexMatrix expectedSquareTranspose = { { 1, 3 }, { 2, 4 } };
    EXPECT_EQ(transpose(square), expectedSquareTranspose);
    ComplexMatrix nonSquare = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
    ComplexMatrix expectedNonSquareTranspose = { { 1, 3, 5 }, { 2, 4, 6 } };
    EXPECT_EQ(transpose(nonSquare), expectedNonSquareTranspose);
}

TEST_F(GeneralAlgebraUnitTests, submatrixDeletionObtained)
{
    ComplexMatrix M1 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
    ComplexMatrix expectedSubmatrix = { { 1, 2 }, { 7, 8 } };
    EXPECT_EQ(extractSubmatrix(M1, { 1 }, { 2 }), expectedSubmatrix);
    EXPECT_THROW(extractSubmatrix(M1, { 10 }, { 2 }), std::out_of_range);
    EXPECT_THROW(extractSubmatrix(M1, { 1 }, { 2, 10 }), std::out_of_range);
    EXPECT_THROW(extractSubmatrix(M1, { 1 }, { 10, 2 }), std::out_of_range);
}
